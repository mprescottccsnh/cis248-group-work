import java.util.Scanner;

public class ParseStrings {

    //GLOBALS
    public static Scanner scnr = new Scanner(System.in);
    public static String userInput, firstString, secondString;
    public static int index;
    public static void promptUser(){
        System.out.println("Enter input string:");
        userInput = scnr.nextLine();
    }

    public static void stringSplit(){
        do {
            promptUser();
            if(!userInput.contains(",") && !userInput.equals("q")){
                System.out.println("Error: No comma in string.\n");
            }else if(userInput.equals("q")){
                return;
            }
        }while (!userInput.contains(","));
        userInput = removeSpaces(userInput);
        index = userInput.indexOf(',');
        firstString = userInput.substring(0, index);
        secondString = userInput.substring(index + 1);
        System.out.println("First word: " + firstString);
        System.out.println("Second word: " + secondString + "\n");
    }

    public static String removeSpaces(String input){
        String output = input;
        do {
            output = output.replaceAll(" ", "");
        }while(output.contains(" "));
        return output;
    }
    public static void main(String[] args) {
        do{
            stringSplit();
        }while(!userInput.equals("q"));
    }
}